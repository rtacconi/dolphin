require 'chef/knife'
require 'chef/knife/bootstrap'

Chef::Config.from_file(File.join('.chef', 'knife.rb'))
class MyCLI
  include Mixlib::CLI
end

MyCLI.option(:ssh_user, 'vagrant')
MyCLI.option(:use_sudo, true)
MyCLI.option(:ssh_port, 2222)
MyCLI.option(:ssh_identity_file, '/Users/riccardo.tacconi/.vagrant.d/insecure_private_key')

#instantiate knife object and add the disable-editing flag to it
knife = Chef::Knife.new
knife.options = MyCLI.options

#set up client creation arguments and run
args = ['bootstrap', '127.0.0.1']
opts = {
#  ssh_user: 'vagrant',
  use_sudo: true,
#  ssh_port: 2222,
#  ssh_identity_file: '/Users/riccardo.tacconi/.vagrant.d/insecure_private_key'
}
new_client = Chef::Knife.run(args, opts)

#
# Cookbook Name:: percona-cluster
# Recipe:: default
#
# Copyright (C) 2014 Operations Engineering
#
# All rights reserved - Do Not Redistribute
#
require 'pty'
require 'expect'

include_recipe "database::mysql"

apt_repository "percona" do
  uri "http://repo.percona.com/apt"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keys.gnupg.net"
  key "1C4CBDCDCD2EFD2A"
end

apt_package "percona-xtradb-cluster-56" do
  action :install
  # notifies :start, "service[mysql]", :immediately
end

service "mysql" do
  action :nothing
  supports :status => true, :start => true, :stop => true, :restart => true, :reload => true
end

cluster = data_bag_item('clusters',node['percona-cluster']['name'])
log cluster.inspect
bind_nic = cluster['mysql_nic']
nic_ip = node["network"]["interfaces"][bind_nic]["addresses"].keys[1]

cluster["primary"] = nic_ip if cluster["gcomm"].empty?
cluster["gcomm"].push(nic_ip) unless cluster["gcomm"].include?(nic_ip)
cluster.save
gcomm = data_bag_item('clusters','cluster1')["gcomm"].join(',') unless cluster["primary"] == nic_ip

template "/etc/mysql/my.cnf" do
  source "my.cnf.erb"
  mode 0644
  owner "root"
  group "root"
  variables({
    gcomm: gcomm
  })
  notifies :restart, "service[mysql]", :immediately
end

if cluster["primary"] == nic_ip
  unless cluster['db_root_password_set']
    log "updating mysql root password"
    connection_hash = {
      :host     => 'localhost',
      :username => 'root',
      :password => ''
    }
    sql_cmd = "UPDATE mysql.user SET password=PASSWORD('#{cluster['db_root_password']}')" \
              " WHERE user='root';" \
              ' flush privileges;'

    mysql_database 'set root password' do
      connection connection_hash
      sql        sql_cmd
      action     :query
    end

    cluster['db_root_password_set'] = true
    cluster.save
  end

  connection_hash = {
    :host     => 'localhost',
    :username => 'root',
    :password => cluster['db_root_password']
  }

  mysql_database_user 'sstuser' do
    connection connection_hash
    password 's3cretPass'
    host 'localhost'
    privileges [:reload, 'LOCK TABLES', 'REPLICATION CLIENT']
    action :grant
  end

  cluster['dbs'].each do |db|
    mysql_database_user db['db_user'] do
      connection connection_hash
      password db['db_password']
      database_name db['db_name']
      host '%'
      privileges [:all]
      require_ssl false
      connection_limit '5'
      action :grant
    end

    mysql_database db['db_name'] do
      connection connection_hash
      owner db['db_user']
      action :create
    end

    mysql_database 'flush the privileges' do
      connection connection_hash
      sql        'flush privileges'
      action     :query
    end
  end
end

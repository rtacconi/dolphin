require 'mixlib/shellout'
require 'securerandom'

v_chef_up = Mixlib::ShellOut.new("vagrant up chef")
v_chef_up.run_command
puts v_chef_up.stdout

require 'net/http'
require 'chef/rest'
require 'chef/config'
require 'chef/data_bag'
require 'chef/data_bag_item'

Chef::Config.from_file(File.join('.chef', 'knife.rb'))

begin
  data_bag = Chef::DataBag.load('clusters')
rescue Net::HTTPServerException => e
  data_bag = Chef::DataBag.new
  data_bag.send(:name, 'clusters')
  data_bag.save
end

begin
  item = Chef::DataBagItem.load('clusters', 'cluster1')
rescue Net::HTTPServerException => e
  item = Chef::DataBagItem.new
end

item.send(:data_bag, 'clusters')
item['id'] = 'cluster1'
item['gcomm'] = []
item['mysql_nic'] = 'eth1'
item['mysql_root_password'] = 'root'
item['primary'] = '192.168.50.11'
item['db_root_password'] ||= SecureRandom.base64
item['db_root_password_set'] = false
item['dbs'] = []
item['dbs'] << {}
item['dbs'].first['db_name'] = 'cluster1db'
item['dbs'].first['db_user'] = 'cluster1_user'
item['dbs'].first['db_password'] ||= SecureRandom.base64
item.save
p item

require 'chef/cookbook_loader'
require 'chef/knife/cookbook_upload'

up = Chef::Knife::CookbookUpload.new
up.config[:cookbook_path] = './cookbooks'
up.config[:all] = true
up.run

nodes = %w{db1}

nodes.each do |host|
  sh = Mixlib::ShellOut.new("vagrant up #{host}")
  sh.run_command
  puts sh.stdout
end

nodes.each do |host|
  sh = Mixlib::ShellOut.new("vagrant provision #{host}")
  sh.run_command
  puts sh.stdout
end

# --------------------------------------------
# running zero.sh
# --------------------------------------------
sudo apt-get -y install build-essential
sudo apt-get -y install software-properties-common python-software-properties
sudo add-apt-repository -y ppa:brightbox/ruby-ng
sudo apt-get update
sudo apt-get -y install ruby2.1 ruby2.1-dev
sudo gem install chef chef-zero --no-ri --no-rdoc
sudo echo 'chef-zero -H 192.168.50.10 &' > /etc/rc.local
sudo sh /etc/rc.local

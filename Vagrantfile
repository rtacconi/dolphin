# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"
Vagrant.require_version ">= 1.5.0"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.omnibus.chef_version = :latest

  config.vm.define "chef" do |db1|
    db1.vm.hostname = "chef"
    db1.vm.box = "chef/ubuntu-14.04"
    db1.vm.network "private_network", ip: "192.168.50.10"
    db1.vm.network "forwarded_port", guest: 8889, host: 8889
    # start chef-zero on 8889 port
    db1.vm.provision "shell", inline: 'sh /vagrant/zero.sh'
  end

  config.vm.define "boot" do |boot|
    boot.vm.hostname = "chef"
    boot.vm.box = "ubuntu/trusty64"
    boot.vm.network "private_network", ip: "192.168.50.20"
  end

  config.vm.define "db1" do |db1|
    db1.vm.hostname = "cluster1-1"
    db1.vm.box = "chef/ubuntu-14.04"
    db1.vm.network "private_network", ip: "192.168.50.11"
    db1.vm.provision :chef_client do |chef|
      chef.chef_server_url = "http://192.168.50.10:8889/"
      chef.validation_key_path = ".chef/stickywicket.pem"
      chef.verbose_logging = true
      chef.json = {
        'percona-cluster' => {
          name: 'cluster1',
          primary: true,
          ipaddress: "192.168.50.11",
          'primary-ip' => "192.168.50.11"
        }
      }
      chef.run_list = [
          "recipe[percona-cluster::default]"
      ]
    end
  end

  config.vm.define "db2" do |db2|
    db2.vm.hostname = "cluster1-2"
    db2.vm.box = "chef/ubuntu-14.04"
    db2.vm.network "private_network", ip: "192.168.50.12"
    db2.vm.provision :chef_client do |chef|
      chef.chef_server_url = "http://192.168.50.10:8889/"
      chef.validation_key_path = ".chef/stickywicket.pem"
      chef.verbose_logging = true
      chef.json = {
        'percona-cluster' => {
          name: 'cluster1',
          primary: false,
          ipaddress: "192.168.50.12",
          'primary-ip' => "192.168.50.11"
        }
      }
      chef.run_list = [
          "recipe[percona-cluster::default]"
      ]
    end
  end

  config.vm.define "db3" do |db3|
    db3.vm.hostname = "cluster1-3"
    db3.vm.box = "chef/ubuntu-14.04"
    db3.vm.network "private_network", ip: "192.168.50.13"
    db3.vm.provision :chef_client do |chef|
      chef.chef_server_url = "http://192.168.50.10:8889/"
      chef.validation_key_path = ".chef/stickywicket.pem"
      chef.verbose_logging = true
      chef.json = {
        'percona-cluster' => {
          name: 'cluster1',
          primary: false,
          ipaddress: "192.168.50.13",
          'primary-ip' => "192.168.50.11"
        }
      }
      chef.run_list = [
          "recipe[percona-cluster::default]"
      ]
    end
  end

  # config.berkshelf.enabled = true


end

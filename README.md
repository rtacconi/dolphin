You need Virtualbox on Linux or OSX. Then download Chef DK https://downloads.chef.io/chef-dk/.

The install rvm:

```bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable
```

Install Rubinius and create the dolphin gemset:

```ruby
rvm install 2.1
rvm use 2.1
rvm gemset create dolphin
```

CD into the directory and run bundler, than you can run workflow.sh shell script to create the project. workflow.sh creates a VM with Chef zero and then it creates the cluster with 3 servers. At the moment there is no testing, neither ChefSpec and serverspec.

File structure:
```bash
Vagrantfile - configures, network, VMs and provisioners
bootstrap.rb - I do not think it is use, it should bootstrap a node without knife (it's pure ruby knife)
cookbooks - all used cookbooks are there, there is no Berkshelf, just manual dependency
cookbooks/percona-cluster - our cookbook to create the 3-node cluster
knife.rb - knife config
data.rb - creates a data bags needs by the percona-cluster cookbook, it is called from workflow.sh
export.sh - it you change a cookbook, run this command to upload the cookbooks to Chef zero
teardown.sh - destroy all nodes
validation.pem - used by knife.rb
zero.sh - bash script to install and run chef zero, it is run from vagrantfile, see chef node
environments - not sure if I am using it
data_bags - not sure if I am using it
```
